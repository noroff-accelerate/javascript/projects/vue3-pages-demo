/**
 * Dependencies
 * @ignore
 */
import { createStore } from 'vuex'

/**
 * Store
 * @ignore
 */
const store = createStore({
  state: {
    todos: [],
  },
  mutations: {
    createTodo: (state, payload) => {
      state.todos.push(payload)
    },
    deleteTodo: (state, payload) => {
      const index = state.todos.indexOf(payload)
      state.todos.splice(index, 1)
    },
  },
})

/**
 * Exports
 * @ignore
 */
export default store
