/**
 * Dependencies
 * @ignore
 */
import { createRouter, createWebHistory } from 'vue-router'

/**
 * Routes
 * @ignore
 */
const routes = [
  {
    name: 'hello',
    path: '/',
    component: () => import('./views/Hello.vue'),
  },
  {
    name: 'person',
    path: '/people/:person',
    component: () => import('./views/Person.vue'),
  },
  {
    name: 'todos',
    path: '/todo',
    component: () => import('./views/Todo.vue'),
  },
]

/**
 * Exports
 * @ignore
 */
export default createRouter({
  history: createWebHistory(
    process.env.NODE_ENV === 'production'
      ? '/javascript/projects/vue3-pages-demo/'
      : '/'
  ),
  routes,
})
